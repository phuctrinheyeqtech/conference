// Express
const express = require("express");
const app = express();

// Http Serveer
const http = require("http");
const server = http.createServer(app);

// Kurento
const kurento = require("kurento-client");

// Get argument value
const minimist = require("minimist");

// Id for Room
const { v4: uuidv4 } = require("uuid");

// Socket.io
const socketIO = require("socket.io");
const io = new socketIO.Server(server, {
  pingTimeout: 1000 * 60 * 3,
  pingInterval: 1000 * 60 * 5,
  cors: {
    origin: "http://localhost:5000",
    methods: ["GET", "POST"],
  },
});

const argv = minimist(process.argv.slice(2), {
  default: {
    as_uri: "http://localhost:3000",
    ws_uri: "ws://localhost:8888/kurento",
  },
});

// Some initial value
let kurentoClient = null;
const iceCandidateQueues = {};
const roomList = {};

io.on("connection", (socket) => {
  console.log(`A CLIENT CONNECTED: ${socket.id}`);

  socket.on("disconnect", (reason) => {
    console.log(`Disconnect ${socket.id} because ${reason}`);
  });

  socket.on("create-room", (roomSize, callback) => {
    console.log(`RECEIVED CREATE_ROOM MESSAGE FROM CLIENT`);
    createRoom(socket, roomSize, (error, myRoom) => {
      if (error) {
        console.error(error);
        return callback("Something bad happened in server");
      } else {
        return joinRoom(socket, myRoom);
      }
    });
  });

  socket.on("join-room", (roomId, callback) => {
    console.log(`RECEIVED JOIN_ROOM MESSAGE FROM CLIENT`);
    getRoom(roomId, (error, myRoom) => {
      if (error) {
        return callback(error);
      } else {
        socket.join(roomId);
        joinRoom(socket, myRoom);
      }
    });
  });

  socket.on("complete-get-my-details", (userId, roomId) => {
    console.log("USER GOT HIS DETAILS");
    const myRoom = roomList[roomId];
    console.log("SEND NEW USER ID TO ALL USER");
    socket.to(myRoom.roomId).emit("get-new-user", userId);

    const existingsParticipantList = [];
    for (const k in myRoom.participants) {
      const par = myRoom.participants[k];
      if (userId !== par.userId) {
        existingsParticipantList.push({ userId: par.userId });
      }
    }

    console.log("SENDING EXISTING PARTICIPANTS TO NEW USER");
    socket.emit("get-existing-participants", userId, existingsParticipantList);
  });

  socket.on("connect-media", (senderId, roomId, sdpOffer) => {
    console.log("RECEIVE CONNECT_MEDIA MESSAGE");
    getEndpointForUser(socket, roomId, senderId, (error, endpoint) => {
      if (error) {
        console.error(error);
      } else {
        endpoint.processOffer(sdpOffer, (error, sdpAnswer) => {
          if (error) return console.error(error);
          console.log("Process Offer Complete");
          socket.emit("receive-video-answer", senderId, sdpAnswer);
        });
      }
    });
  });

  socket.on("client-send-candidate", (senderId, roomId, iceCandidate) => {
    const user = roomList[roomId].participants[socket.id];
    if (!user) {
      console.error("What the hell???");
      return;
    } else {
      const candidate = getComplexTypeCandidate(iceCandidate);
      if (senderId === user.userId) {
        if (user.outgoingMedia) {
          user.outgoingMedia.addIceCandidate(candidate);
        } else {
          if (!iceCandidateQueues[user.userId]) {
            iceCandidateQueues[user.userid] = [];
          }
          iceCandidateQueues[user.userId].push({ candidate });
        }
      } else {
        if (user.incomingMedia[senderId]) {
          user.incomingMedia[senderId].addIceCandidate(candidate);
        } else {
          if (!iceCandidateQueues[senderId]) {
            iceCandidateQueues[senderId] = [];
          }
          iceCandidateQueues[senderId].push({ candidate });
        }
      }
    }
  });

  socket.on("user-leave-room", () => {});
});

function getRoom(roomId, callback) {
  if (!roomList[roomId]) {
    return callback("Room not found");
  } else {
    return callback(null, roomList[roomId]);
  }
}

function joinRoom(socket, myRoom, callback) {
  myRoom.pipeline.create("WebRtcEndpoint", (error, outgoingMedia) => {
    if (error) {
      console.error(error);
      callback("Something bad happened");
    } else {
      const user = {
        userId: socket.id,
        outgoingMedia,
        incomingMedia: {},
      };

      const userCandidateQueue = iceCandidateQueues[socket.id];
      if (userCandidateQueue) {
        while (userCandidateQueue.length > 0) {
          const ice = userCandidateQueue.shift();
          user.outgoingMedia.addIceCandidate(ice.candidate);
        }
      }

      outgoingMedia.on("OnIceCandidate", (e) => {
        const candidate = getComplexTypeCandidate(e.candidate);
        socket.emit("receive-server-candidate", user.userId, candidate);
      });

      const iceCandidateQueue = iceCandidateQueues[user.userId];
      if (iceCandidateQueue) {
        while (iceCandidateQueue.length > 0) {
          const ice = iceCandidateQueue.shift();
          user.outgoingMedia.addIceCandidate(ice.candidate);
        }
      }

      socket.emit("get-my-details", user.userId, myRoom.roomId);
      myRoom.participants[user.userId] = user;
    }
  });
}

function getEndpointForUser(socket, roomId, senderId, callback) {
  const myRoom = roomList[roomId];
  console.log(Object.keys(myRoom.participants));
  console.log(`sender: ${senderId}`);
  console.log(`current: ${socket.id}`);
  
  const asker = myRoom.participants[socket.id];
  const sender = myRoom.participants[senderId];

  if (asker.userId === senderId) {
    return callback(null, asker.outgoingMedia);
  }
  if (asker.incomingMedia[senderId]) {
    sender.outgoingMedia.connect(asker.incomingMedia[senderId], (error) => {
      if (error) {
        return callback(error);
      } else {
        return callback(null, asker.incomingMedia[sender]);
      }
    });
  } else {
    myRoom.pipeline.create("WebRtcEndpoint", (error, incoming) => {
      if (error) {
        return callback(error);
      } else {
        asker.incomingMedia[senderId] = incoming;

        let iceCandidateQueue = iceCandidateQueues[senderId];
        if (iceCandidateQueue) {
          while (iceCandidateQueue.length > 0) {
            const ice = iceCandidateQueue.shift();
            incoming.addIceCandidate(ice.candidate);
          }
        }

        incoming.on("OnIceCandidate", (e) => {
          const candidate = getComplexTypeCandidate(e.candidate);
          socket.emit("receive-server-candidate", senderId, candidate);
        });

        sender.outgoingMedia.connect(incoming, (error) => {
          if (error) {
            return callback(error);
          } else {
            return callback(null, incoming);
          }
        });
      }
    });
  }
}

/**
 * Get kurentoClient.
 * Create room, room pipeline.
 * @param {*} socket
 * @param {string} hostname
 * @param {number} maxSize
 * @param {(error: Error, myRoom: {
 *    roomId: string,
 *    hostId,
 *    size: number,
 *    maxSize: number,
 *    pipeline: kurento.MediaPipeline
 *    participants: {
 *        [k:string]: {
 *          userId: string,
 *          incomingMedia:kurento.WebRtcEndpoint
 *          outgoing:kurento.WebRtcEndpoint
 *        }
 *      }
 *    }
 * }) => void} cb
 */
function createRoom(socket, maxSize, cb) {
  getKurentoClient((error, kurentoClient) => {
    if (error) {
      console.error(error);
      cb("Something happened in the server.");
    } else {
      const myRoom = {
        roomId: uuidv4(),
        hostId: socket.id,
        size: 1,
        maxSize: maxSize,
        pipeline: null,
        participants: {},
        mediapipeline: null,
      };

      roomList[myRoom.roomId] = myRoom;
      socket.join(myRoom.roomId);

      kurentoClient.create("MediaPipeline", (error, pipeline) => {
        if (error) {
          console.error(error);
          return cb("Something happened in the server.");
        } else {
          myRoom.pipeline = pipeline;
          cb(null, myRoom);
        }
      });
    }
  });
}
//
//
//
//
async function __test() {
  const kurentoClient = await kurento("asd");
  const pipeline = await kurentoClient.create("MediaPipeline");
  const endpoint = await pipeline.create("WebRtcEndpoint");
  endpoint.addIceCandidate();
  endpoint.on("OnIceCandidate", (e) => {
    e.candidate();
  });
  endpoint.setMinVideoSendBandwidth;
}

//
//
//
//
//
// UTILS
/**
 *
 * @param {{roomId: string, host: {id:string, name: string}, size: number, participants: {}}} updates
 */
function updateRoom(roomId, updates) {
  if (typeof roomList[roomId] === "undefined") {
    return;
  } else {
    for (const k in roomList[roomId]) {
      if (typeof updates[k] !== "undefined") {
        myRoom[k] = updates[k];
      }
    }
  }
}

function getComplexTypeCandidate(candidate) {
  return kurento.register.complexTypes.IceCandidate(candidate);
}

function getKurentoClient(cb) {
  if (kurentoClient !== null) {
    return cb(null, kurentoClient);
  } else {
    return kurento(argv.ws_uri, (error, _client) => {
      if (error) {
        callback(error);
      } else {
        kurentoClient = _client;
        cb(null, _client);
      }
    });
  }
}

function getRoomSize(roomId) {
  return io.sockets.adapter.rooms.get(roomId).size;
}

app.get("*", (req, res) => {
  return res.json({ ok: "Hello Worlds" });
});

server.listen(3000, () => {
  console.log("SERVER LISTENING http://localhost:3000");
});
