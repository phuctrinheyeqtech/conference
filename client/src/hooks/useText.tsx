import * as React from "react";

export default function useText(
  initialText: string
): [
  string,
  (e: React.ChangeEvent<HTMLInputElement> | string) => void,
  () => void
] {
  const [text, setText] = React.useState(initialText);

  function handleTextChange(e: React.ChangeEvent<HTMLInputElement> | string) {
    if (typeof e === "string") {
      setText(e);
    } else {
      setText(e.target.value);
    }
  }

  function handleReset() {
    setText("");
  }

  return [text, handleTextChange, handleReset];
}
