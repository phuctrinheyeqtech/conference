import * as React from "react";
import io from "socket.io-client";
import { CLIENT, SERVER } from "../constants/events";
import { setParticipants, setRoomId, setUserId } from "../constants/actions";
import { createVideo } from "../utils";
import iceServers from "../constants/ice-server.json";
import kurentoUtils from "kurento-utils";
import { useRoom } from "../lib/RoomContext";
import { useNavigate } from "react-router-dom";
import {socket} from "../";

export function useSocket() {
  const navigate = useNavigate();
  const {
    room: { roomId, participants, userId },
    dispatch,
  } = useRoom();

  React.useEffect(() => {
    function onGettingUser(newcomerId: string) {
      console.log("GOT NEW USER");
      const video = createVideo(newcomerId);

      const options = {
        remoteVideo: video,
        onicecandidate(candidate: RTCIceCandidate) {
          socket.emit("client-send-candidate", newcomerId, roomId, candidate);
        },
        iceServers,
      };

      const rtcPeer = kurentoUtils.WebRtcPeer.WebRtcPeerRecvonly(
        options,
        function (error) {
          if (error) {
            console.error(error);
          } else {
            //@ts-ignore
            this.generateOffer(function onOffer(error: Error, sdpOffer: any) {
              if (error) {
                console.error(error);
              } else {
                socket.emit(SERVER.CONNECT_MEDIA, newcomerId, roomId, sdpOffer);
              }
            });
          }
        }
      );

      dispatch(
        setParticipants({ [newcomerId]: { id: newcomerId, rtcPeer, video } })
      );
    }

    if (roomId?.length !== 0 && userId?.length !== 0) {
      socket.emit("complete-get-my-details", userId, roomId);
      navigate(`room/${roomId}`);
    }

    socket.on(CLIENT.GET_MY_DETAILS, (userId, roomId) => {
      dispatch(setRoomId(roomId));
      dispatch(setUserId(userId));
    });

    if (roomId?.length === 0) return;

    socket.on(CLIENT.GET_NEW_USER, onGettingUser);

    socket.on(
      CLIENT.GET_EXISTING_PARTICIPANTS,
      function (myId: string, existingUsers: Array<{ userId: string }>) {
        const video = createVideo(myId);

        const options = {
          localVideo: video,
          mediaConstraints: {
            video: {
              mandatory: { maxFrameRate: 15, minFrameRate: 15, maxWidth: 320 },
            },
            audio: true,
          },
          onicecandidate(candidate: any) {
            sendCandidate(myId, roomId!, candidate);
          },
          iceServers,
        };

        const rtcPeer = kurentoUtils.WebRtcPeer.WebRtcPeerSendonly(
          options,
          function (error) {
            if (error) {
              console.error(error);
            } else {
              //@ts-ignore
              this.generateOffer((error: Error, sdpOffer: string) => {
                if (error) {
                  console.error(error);
                } else {
                  sendOffer(myId, roomId!, sdpOffer);
                }
              });
            }
          }
        );

        existingUsers.forEach(({ userId }) => {
          onGettingUser(userId);
        });

        //@ts-ignore
        dispatch(setParticipants({ [myId]: { id: myId, rtcPeer, video } }));
      }
    );

    socket.on(
      CLIENT.RECEIVE_VIDEO_ANSWER,
      function (userId: string, sdpAnswer: string) {
        //@ts-ignore
        participants[userId].rtcPeer?.processAnswer(sdpAnswer, (error) => {
          if (error) {
            console.error(error);
          } else {
            console.log("Complete Handshake");
          }
        });
      }
    );

    socket.on(CLIENT.RECEIVE_SERVER_CANDIDATE, (userId, candidate) => {
      //@ts-ignore
      participants[userId].rtcPeer?.addIceCandidate(candidate);
    });

    return () => {
      socket.removeAllListeners();
    }
  }, [roomId]);
}

export function sendCandidate(userId: string, roomId: string, candidate: any) {
  socket.emit(SERVER.RECEIVE_CLIENT_CANDIDATE, userId, roomId, candidate);
}

export function sendOffer(userId: string, roomId: string, sdpOffer: string) {
  socket.emit(SERVER.CONNECT_MEDIA, userId, roomId, sdpOffer);
}

export function createRoom(size: number) {
  socket.emit(SERVER.CREATE_ROOM, size);
}

export function joinRoom(roomId: string) {
  socket.emit(SERVER.JOIN_ROOM, roomId);
}
