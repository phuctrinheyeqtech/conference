import * as React from "react";
import { Link } from "react-router-dom";
import FormHeader from "../components/FormHeader";
import { createRoom } from "../hooks/useSocket";
import useText from "../hooks/useText";

export default function AdminPage() {
  const [email, setEmail, resetEmail] = useText("");
  const [memberList, setMemberList] = React.useState<string[]>([]);
  const [roomSize, setRoomSize] = React.useState(6);

  function addMembers() {
    if (email.trim().length === 0) {
      return;
    }
    if (memberList.includes(email.trim())) {
      return;
    } else {
      setMemberList([...memberList, email]);
      resetEmail();
    }
  }

  function handleSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    createRoom(roomSize);
  }

  return (
    <div className="flex flex-col items-center justify-center h-screen text-gray-900 bg-gradient-to-br from-gray-300 via-teal-700 to-gray-800">
      <form
        onSubmit={handleSubmit}
        className="flex flex-col gap-4 border-black border-2 p-4 rounded shadow-md shadow-black"
      >
        <FormHeader title="Create Room" />

        <div>
          <div className="flex items-center gap-2 justify-between">
            <label htmlFor="roomSize">Room Size:</label>
            <div className="w-60 rounded border focus-within:border-gray-900 overflow-hidden flex outline-none  bg-white">
              <input
                id="roomSize"
                placeholder="6"
                value={roomSize}
                onChange={(e) => setRoomSize(Number(e.target.value))}
                className="border-none outline-none w-full p-2"
                type="number"
                min={2}
                max={500}
              />
            </div>
          </div>
        </div>

        <div>
          <div className="flex items-center gap-2 justify-between">
            <label htmlFor="member">Invite member:</label>
            <div className="w-60 rounded border focus-within:border-gray-900 overflow-hidden flex outline-none  bg-white">
              <input
                id="member"
                placeholder="member email"
                className="border-none outline-none w-full p-2 m-1"
                type="text"
                value={email}
                onChange={setEmail}
              />
              <button
                onClick={addMembers}
                type="button"
                className="uppercase font-semibold p-2 border-l border-black"
              >
                Add
              </button>
            </div>
          </div>
        </div>

        <div className=" bg-gray-200 h-20 rounded border border-black overflow-hidden">
          <div className="p-2 h-full overflow-y-scroll">
            {memberList.length > 0 ? (
              memberList.map((member, i) => (
                <div key={member}>
                  {i + 1}: {member}
                </div>
              ))
            ) : (
              <p className="text-center text-lg text-zinc-500">
                Attendants' emails will appear here
              </p>
            )}
          </div>
        </div>

        <button
          type="submit"
          className="p-2 border border-black rounded bg-gray-200 text-base uppercase font-semibold"
        >
          Create Room
        </button>

        <Link
          className="p-2 border text-center border-black rounded bg-gray-200 text-base uppercase font-semibold"
          to="/"
        >
          Go Back
        </Link>
      </form>
    </div>
  );
}
