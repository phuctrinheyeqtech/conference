import * as React from "react";
import { Link } from "react-router-dom";
import FormHeader from "../components/FormHeader";
import { joinRoom } from "../hooks/useSocket";
import useText from "../hooks/useText";

export default function HomePage() {
  const [roomId, setRoomId, resetRoomId] = useText("");
  const [error, setError] = React.useState("");

  function isFormValid() {
    if (roomId.length < 1) {
      setError("Room id is required");
      return false;
    } else {
      return true;
    }
  }

  function handleSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    if (isFormValid()) {
      joinRoom(roomId);
    }
  }

  return (
    <div className="flex flex-col items-center justify-center h-screen text-gray-900 bg-gradient-to-br from-gray-300 via-teal-700 to-gray-800">
      <div>
        <form
          onSubmit={handleSubmit}
          className="flex flex-col gap-4 border-black border-2 p-4 rounded shadow-md shadow-black"
        >
          <div>
            <FormHeader title="Conference" />
          </div>

          <div>
            <div className="flex items-center gap-2 justify-between">
              <label htmlFor="roomId">Room Id:</label>
              <div className="w-60 rounded border focus-within:border-gray-900 overflow-hidden flex outline-none  bg-white">
                <input
                  id="roomId"
                  placeholder="room id"
                  className="border-none outline-none w-full p-2"
                  type="text"
                  value={roomId}
                  onChange={setRoomId}
                  onFocus={e => setError("")}
                />
                {roomId.length > 0 ? (
                  <button className="p-2" type="button" onClick={resetRoomId}>
                    ❌
                  </button>
                ) : null}
              </div>
            </div>
            {error.length > 0 ? (
              <p className="bg-red-400 p-1 mt-1 text-center">{error}</p>
            ) : null}
          </div>

          <button
            type="submit"
            className="p-2 border border-black rounded bg-gray-200 text-base uppercase font-semibold"
          >
            Join
          </button>

          <Link
            className="p-2 border text-center border-black rounded bg-gray-200 text-base uppercase font-semibold"
            to="/admin"
          >
            I am admin
          </Link>
        </form>
      </div>
    </div>
  );
}
