import * as React from "react";
import CallUtils from "../components/layouts/CallUtils";
import Sidebar from "../components/Sidebar";
import VideoDisplay from "../components/VideoDisplay";
import { useRoom } from "../lib/RoomContext";

export default function RoomPage() {
  const {
    room: { participants },
  } = useRoom();
  const [focus, setFocus] = React.useState({ isFocus: false, focusOn: "" });

  return (
    <div className="h-screen w-screen flex overflow-hidden relative bg-slate-800">
      {/* Sidebar Chat */}
      <Sidebar />

      {/* Main Screen */}
      <div className="h-full w-full px-8 py-4 flex-grow">
        <div className="grid grid-cols-3 gap-4 grid-rows-2 h-full w-full">
          {/* Video Display */}
          {Object.values(participants!).map((user) => (
            <VideoDisplay
              focus={focus}
              setFocus={setFocus}
              user={user}
              key={user.id}
            />
          ))}
        </div>
      </div>

      {/* Video Feature Section */}
      <CallUtils />
    </div>
  );
}
