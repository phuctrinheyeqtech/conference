
export function createVideo(userId: string) {
  const video = document.createElement("video");
  video.autoplay = true;
  video.className = "w-full h-full";
  const username = document.createElement("p");
  username.textContent = userId;
  return video;
}

