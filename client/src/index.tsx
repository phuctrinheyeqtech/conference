import * as React from "react";
import * as serviceWorker from "./serviceWorker";
import ReactDOM from "react-dom/client";
import "./styles/tailwind.css";
import App from "./App";
import { BrowserRouter } from "react-router-dom";
import RoomProvider from "./lib/RoomContext";

import { io } from "socket.io-client";
export const socket = io("http://localhost:3000");

socket.on("disconnect", (reason, description) => {
  console.error(reason);
  if (description) console.error(description);
});

ReactDOM.createRoot(document.getElementById("root")!).render(
  <BrowserRouter>
    <RoomProvider>
      <App />
    </RoomProvider>
  </BrowserRouter>
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
