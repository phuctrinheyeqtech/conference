import type { WebRtcPeer } from "kurento-utils";
// import { Socket } from "socket.io-client";

export type Room = {
  userId?: string;
  isRoomHost?: boolean;
  roomId?: string;
  participants?: {
    [id: string]: Participant;
  };
};

export type RoomContextType = {
  // socket: Socket
  room: Room;
  dispatch: React.Dispatch<RoomAction>;
};

export type RoomAction = {
  type: string;
  payload: Room;
};

export type Participant = {
  id: string;
  rtcPeer: WebRtcPeer | null;
  video: HTMLVideoElement | null;
};

export type Participants = {
  [k: string]: Participant;
};

export type UserList = { userId: string }[];
