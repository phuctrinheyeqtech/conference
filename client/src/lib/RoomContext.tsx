import * as React from "react";
import { io } from "socket.io-client";
import ACTIONS, { setParticipants } from "../constants/actions";
import { CLIENT, SERVER } from "../constants/events";
import type { RoomAction, Room, RoomContextType } from "../types";
// import { createVideo } from "../utils";
//
// import iceServers from "../constants/ice-server.json";
// import kurentoUtils from "kurento-utils";

const initialState: Room = {
  userId: "",
  isRoomHost: false,
  roomId: "",
  participants: {},
};

// const socket = io("http://localhost:3000");

const RoomContext = React.createContext<RoomContextType>({
  // socket,
  room: initialState,
  dispatch: (value) => {},
});
export const useRoom = () => React.useContext(RoomContext);

function roomReducer(state: Room, action: RoomAction): Room {
  switch (action.type) {
    case ACTIONS.SET_IS_ROOM_HOST:
      return { ...state, isRoomHost: action.payload.isRoomHost };
    case ACTIONS.SET_USER_ID:
      return { ...state, userId: action.payload.userId };
    case ACTIONS.SET_ROOM_ID:
      return { ...state, roomId: action.payload.roomId };
    case ACTIONS.SET_PARTICIPANTS:
      console.log(state.participants);
      console.log(action.payload.participants);
      return {
        ...state,
        participants: { ...state.participants, ...action.payload.participants },
      };
    default:
      throw new Error(`Invalid action type: ${action.type}`);
  }
}

export default function RoomProvider({ children }: React.PropsWithChildren) {
  const [room, dispatch] = React.useReducer(roomReducer, initialState);

  // React.useEffect(() => {
  //   function receiveVideoFrom(userId: string) {
  //     const video = createVideo(userId);
  //
  //     function onicecandidate(candidate: RTCIceCandidate) {
  //       socket.emit(
  //         SERVER.RECEIVE_CLIENT_CANDIDATE,
  //         room.roomId,
  //         userId,
  //         candidate
  //       );
  //     }
  //
  //     function onOffer(error: Error | string, sdpOffer: string) {
  //       error
  //         ? console.error(error)
  //         : socket.emit(SERVER.CONNECT_MEDIA, userId, room.roomId, sdpOffer);
  //     }
  //
  //     const options = {
  //       remoteVideo: video,
  //       onicecandidate,
  //       iceServers,
  //     };
  //
  //     const rtcPeer = kurentoUtils.WebRtcPeer.WebRtcPeerRecvonly(
  //       options,
  //       function (error) {
  //         if (error) console.error(error);
  //         else {
  //           //@ts-ignore
  //           this.generateOffer(onOffer);
  //         }
  //       }
  //     );
  //
  //     dispatch(setParticipants({ [userId]: { id: userId, video, rtcPeer } }));
  //   }
  //
  //   // ON A NEW USER JOIN ROOM
  //   socket.on(CLIENT.GET_NEW_USER, receiveVideoFrom);
  //
  //   // ON
  //   socket.on(CLIENT.GET_MY_DETAILS, () => {});
  //
  //   // ON GETTING PARTICIPANTS INFO
  //   socket.on(
  //     CLIENT.GET_EXISTING_PARTICIPANTS,
  //     (userId: string, participants: { userId: string }[]) => {
  //       const video = createVideo(userId);
  //       const mediaConstraints = {
  //         audio: true,
  //         video: {
  //           mandatory: { maxWidth: 320, maxFrameRate: 15, minFrameRate: 15 },
  //         },
  //       };
  //       function onicecandidate(candidate: RTCIceCandidate) {
  //         socket.emit(
  //           SERVER.RECEIVE_CLIENT_CANDIDATE,
  //           userId,
  //           room.roomId,
  //           candidate
  //         );
  //       }
  //
  //       const options = {
  //         localVideo: video,
  //         mediaConstraints,
  //         onicecandidate,
  //         iceServers,
  //       };
  //
  //       const rtcPeer = kurentoUtils.WebRtcPeer.WebRtcPeerSendonly(
  //         options,
  //         function (e) {
  //           //@ts-ignore
  //           e ? console.error(e) : this.generateOffer(onOffer);
  //         }
  //       );
  //
  //       function onOffer(e: Error, sdpOffer: string) {
  //         e
  //           ? console.error(e)
  //           : socket.emit(SERVER.CONNECT_MEDIA, userId, room.roomId, sdpOffer);
  //       }
  //
  //       setParticipants({ [userId]: { rtcPeer, video, id: userId } });
  //
  //       participants.forEach(({ userId }) => receiveVideoFrom(userId));
  //     }
  //   );
  //
  //   socket.on(
  //     CLIENT.RECEIVE_SERVER_CANDIDATE,
  //     (userId: string, candidate: RTCIceCandidate) => {
  //       //@ts-ignore
  //       room.participants[userId]!.rtcPeer!.addIceCandidate(candidate);
  //     }
  //   );
  //
  //   socket.on(
  //     CLIENT.RECEIVE_VIDEO_ANSWER,
  //     (userId: string, sdpAnswer: string) => {
  //       //@ts-ignore
  //       room.participants[userId]!.rtcPeer?.processAnswer(sdpAnswer);
  //     }
  //   );
  // }, []);

  return (
    <RoomContext.Provider value={{ room, dispatch }}>
      {children}
    </RoomContext.Provider>
  );
}
