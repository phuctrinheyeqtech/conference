import * as React from "react";

export default function FormHeader({ title }: { title: string }) {
  return (
    <div className="relative flex py-2 items-center">
      <div className="flex-grow border-t border-gray-800"></div>
      <span className="flex-shrink mx-4 text-xl font-bold uppercase">
        {title}
      </span>
      <div className="flex-grow border-t border-gray-800"></div>
    </div>
  );
}
