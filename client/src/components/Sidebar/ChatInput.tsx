import * as React from "react";
import Picker from "emoji-picker-react";
import useText from "../../hooks/useText";
import { HiOutlineCheck } from "react-icons/hi";

export default function ChatInput() {
  const [openPicker, setOpenPicker] = React.useState(false);
  const [message, setMessage, clearMessage] = useText("");

  React.useEffect(() => {
    document.body.addEventListener("click", () => {
      if (openPicker) {
        setOpenPicker(false);
      }
    });
    document.body.removeEventListener("click", () => {
      if (openPicker) {
        setOpenPicker(false);
      }
    });
  }, [openPicker]);

  function onEmojiClick(
    e: React.MouseEvent<Element, MouseEvent>,
    emojiObject: any
  ) {
    setMessage(message + emojiObject.emoji);
  }

  function onSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    if (message.trim().length === 0) {
      return;
    } else {
      console.log(message);
      clearMessage();
    }
  }

  return (
    <form
      onSubmit={onSubmit}
      className="p-1 flex absolute bottom-0 left-0 h-12 w-full bg-white items-center justify-center"
    >
      <div className="relative w-10 aspect-square border-r border-black">
        {openPicker ? (
          <div
            onClick={e => {
              e.stopPropagation();
            }}
            className={`absolute bottom-full z-10 left-full ${""}`}
          >
            <Picker onEmojiClick={onEmojiClick} />
          </div>
        ) : null}
        <button
          type="button"
          onClick={e => {
            e.stopPropagation();
            setOpenPicker(!openPicker);
          }}
          className="absolute top-0 left-0 h-full w-full"
        >
          😂
        </button>
      </div>
      <input
        onChange={e => setMessage(e.target.value)}
        value={message}
        className="p-2 flex-grow outline-none"
        type="text"
      />
      <button
        type="submit"
        className="w-10 aspect-square border-l border-black"
      >
        <HiOutlineCheck className="h-full w-full text-green-500" />
      </button>
    </form>
  );
}
