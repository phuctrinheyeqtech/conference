import * as React from "react";

export default function ChatMessage({
  username,
  message,
}: {
  username: string;
  message: string;
}) {
  return (
    <div className="border-b py-2 border-white">
      <p className="text-white font-semibold">{username} said:</p>
      <p className="text-white text-base px-2">{message}</p>
    </div>
  );
}
