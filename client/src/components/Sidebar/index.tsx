import * as React from "react";
import ChatInput from "./ChatInput";
import ChatMessage from "./ChatMessage";

import { HiChevronDoubleRight } from "react-icons/hi";

const data = [
  {
    username: "John",
    message: `Lorem ipsum dolor, sit amet consectetur adipisicing elit. Saepe,
                veniam adipisci iure perspiciatis, quae tempore aliquid illo
                quam quisquam optio, impedit nulla nihil iusto! Hic consectetur
                pariatur omnis a enim!`,
  },
  {
    username: "Jane",
    message: `Lorem ipsum dolor, sit amet consectetur adipisicing elit. Saepe,
                quam quisquam optio, impedit nulla nihil iusto! Hic consectetur
                pariatur omnis a enim!`,
  },
  {
    username: "Jim",
    message: `Lorem ipsum dolor, sit amet consectetur adipisicing elit. Saepe,
                veniam adipisci iure perspiciatis, quae tempore aliquid illo
                quam quisquam optio, impedit nulla nihil iusto! Hic consectetur`,
  },
  {
    username: "Jose",
    message: `Lorem ipsum dolor, sit amet consectetur adipisicing elit. Saepe,
                veniam adipisci iure perspiciatis, quae tempore aliquid illo
                quam quisquam optio, impedit nulla nihil iusto! Hic consectetur
                pariatur omnis a enim!`,
  },
  {
    username: "John",
    message: `Lorem ipsum dolor, sit amet consectetur adipisicing elit. Saepe,
                veniam adipisci iure perspiciatis, quae tempore aliquid illo
                quam quisquam optio, impedit nulla nihil iusto! Hic consectetur
                pariatur omnis a enim!`,
  },
];

export default function Sidebar() {
  const [openSidebar, setOpenSidebar] = React.useState(true);

  return (
    <div
      className={`relative duration-200 h-full w-96 max-w-sm bg-slate-900 ${
        openSidebar ? "" : "w-0"
      }`}
    >
      <button
        onClick={() => {
          setOpenSidebar(!openSidebar);
        }}
        className={`absolute z-10 border border-gray-200 bg-gray-200 rounded-full left-full top-1/2 -translate-x-1/2 -translate-y-1/2 duration-500 ${
          openSidebar ? "-rotate-180" : ""
        }`}
      >
        <HiChevronDoubleRight className="text-4xl" />
      </button>
      <div
        className={`absolute flex flex-col w-full h-full duration-200 ${
          openSidebar ? "" : "-translate-x-80"
        }`}
      >
        <div className="w-full h-10 p-2 flex border border-white text-white items-center justify-between">
          <p>people: 6</p>
          <p>people: 6</p>
        </div>
        <div className="border border-white flex-grow overflow-y-scroll mb-12 no-scrollbar">
          {/* Chat Display */}
          <div className="h-[calc(100%-3rem)]  p-2 ">
            {data.map(({ username, message }) => (
              <ChatMessage
                key={Math.random()}
                username={username}
                message={message}
              />
            ))}
          </div>

          {/* Chat Input */}
          <ChatInput />
        </div>
      </div>
    </div>
  );
}
