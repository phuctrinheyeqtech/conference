import * as React from "react";
import { Participant } from "../../types";

type PropsType = {
  user: Participant;
  focus: {
    isFocus: boolean;
    focusOn: string;
  };
  setFocus: React.Dispatch<
    React.SetStateAction<{
      isFocus: boolean;
      focusOn: string;
    }>
  >;
};

export default function VideoDisplay({ user, focus, setFocus }: PropsType) {
  React.useEffect(() => {
    if (user.video !== null) {
      const videoWrapper = document.getElementById(user.id) as HTMLDivElement;
      if (videoWrapper.childElementCount === 0) {
        videoWrapper.appendChild(user.video);
      }
    }
  }, []);

  function handleFocus(_: React.MouseEvent<HTMLButtonElement, MouseEvent>) {
    if (focus.isFocus) {
      setFocus({ isFocus: false, focusOn: "" });
    } else {
      setFocus({ isFocus: true, focusOn: user.id });
    }
  }

  return (
    <div className="relative shadow shadow-black w-full h-full border border-black group">
      <button
        onClick={handleFocus}
        className="absolute right-0 top-0 z-10 shadow shadow-black h-10 w-10 aspect-square border border-black p-2 opacity-0 duration-200 group-hover:opacity-100"
      >
        <svg
          width="20"
          height="20"
          viewBox="0 -1 163 163"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <g clipPath="url(#a)" fill="#000">
            <path d="M12 160a719 719 0 0 1 25 0 133 133 0 0 0 18-2l2-2 1-1a1 1 0 0 0 0-1h-1a53 53 0 0 1-3-1 5917 5917 0 0 0-38-2l4-4 11-10a658 658 0 0 0 41-46 4 4 0 0 0-3-2c-4 1-6 3-9 6l-1 1-11 10a686 686 0 0 0-35 35l-2 1a243 243 0 0 1-1-16v-3c-1-5-1-9-4-13a1 1 0 0 0-1 0l-2 1c-2 11-2 22-2 32v5a17 17 0 0 0 2 9c3 3 6 3 9 3ZM143 15a25152 25152 0 0 1-47 45l-6 6-1 2v1a1 1 0 0 0 0 1l2 1h1l2-1 4-3a43552 43552 0 0 0 52-49l1-1h1v4l1 11v2a57 57 0 0 0 2 15c3 0 4-2 4-3 3-11 4-22 3-36 0-7-3-10-10-10l-22 1a96 96 0 0 0-17 3l-1 1-1 1a1 1 0 0 0 0 1c3 2 5 2 8 2h1l4 1h23l-1 1-3 4Z" />
          </g>
          <defs>
            <clipPath id="a">
              <path fill="#fff" transform="translate(1)" d="M0 0h162v161H0z" />
            </clipPath>
          </defs>
        </svg>
      </button>

      <div className="h-full w-full">
        <div id={user.id} className="h-full w-full"></div>
        <p>{user.id}</p>
      </div>
    </div>
  );
}
