import * as React from "react";
import { HiOutlinePhoneMissedCall } from "react-icons/hi";

export default function CallUtils() {
  const [mute, setMute] = React.useState(false);
  const [mic, setMic] = React.useState(false);

  return (
    <div
      className={`absolute -translate-y-1/4 bottom-0 left-1/2 duration-200 -translate-x-1/2 ${"opacity-100"}`}
    >
      <div className="flex border-2 border-gray-200 rounded-full py-2 px-4 gap-10">
        <button
          onClick={() => setMute(!mute)}
          className="w-12 p-2 aspect-square rounded-full bg-gray-200"
        >
          {mute ? (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 489.9 489.9"
              xmlSpace="preserve"
            >
              <path d="M269 70c-5 0-10 2-14 4l-1 1-100 69H73c-40 0-73 33-73 73v56c0 40 33 73 73 73h85l96 69 1 1c4 2 9 4 14 4a28 28 0 0 0 28-29V99c0-16-12-29-28-29zm4 321-1 3-3 1h-1l-99-71c-2-2-5-3-7-3H73c-27 0-48-21-48-48v-56c0-27 21-49 48-49h85l7-2 102-71h2c2 0 4 1 4 4v292zM486 171c-5-4-12-4-17 0l-56 57-57-57c-5-4-12-4-17 0-5 5-5 13 0 18l56 56-56 56a12 12 0 0 0 9 21c3 0 6-1 8-3l57-57 56 57c2 2 5 3 9 3s6-1 8-3c5-5 5-13 0-18l-56-56 56-56c5-5 5-13 0-18z" />
            </svg>
          ) : (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 489.6 489.6"
              xmlSpace="preserve"
            >
              <path d="m361 338 7 2c4 0 8-2 10-5a154 154 0 0 0 0-180c-4-6-11-7-17-3-5 4-7 12-3 17a130 130 0 0 1 0 152c-4 5-2 13 3 17zM425 396l7 3c4 0 8-2 11-6a255 255 0 0 0 0-297c-4-5-12-7-18-3-5 4-6 12-3 17a231 231 0 0 1 1 269c-4 6-3 13 2 17zM255 416c4 2 9 3 14 3v1a28 28 0 0 0 28-29V99a28 28 0 0 0-42-25h-1l-100 70H73c-40 0-73 33-73 73v56c0 40 33 73 73 73h85l96 69 1 1zm-93-95H73c-27 0-48-22-48-48v-56c0-27 21-49 48-49h85l7-2 102-71h2c2 0 4 1 4 3v293l-1 3-3 1h-1l-99-71c-2-2-5-3-7-3z" />
            </svg>
          )}
        </button>

        <button
          onClick={() => setMic(!mic)}
          className="w-12 p-2 aspect-square rounded-full bg-gray-200"
        >
          {mic ? (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 58 58"
              xmlSpace="preserve"
            >
              <path d="m45 28-1 1v6a14 14 0 0 1-28 0v-6a1 1 0 1 0-2 0v6c0 8 6 15 15 16v5h-5a1 1 0 1 0 0 2h12a1 1 0 1 0 0-2h-5v-5c8-1 15-8 15-16v-6l-1-1z" />
              <path d="M30 46c6 0 11-5 11-11V11a11 11 0 0 0-22 0v24c0 6 4 11 11 11zm-9-35a9 9 0 0 1 18 0v24a9 9 0 0 1-18 0V11zM52 4h-1l-9 9a1 1 0 1 0 1 2l9-9V4z" />
              <path d="M37 21a1 1 0 1 0-1-2L22 33a1 1 0 1 0 1 2l14-14zM13 42l-7 7a1 1 0 1 0 1 2l7-7a1 1 0 1 0-1-2z" />
            </svg>
          ) : (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 64 64"
              xmlSpace="preserve"
            >
              <path d="M32 40c6 0 11-5 11-11V11c0-6-5-11-11-11S21 5 21 11v18c0 6 5 11 11 11zm-9-29c0-5 4-9 9-9s9 4 9 9v18c0 5-4 9-9 9s-9-4-9-9V11z" />
              <path d="m50 25-1 1v4c0 9-8 17-17 17s-17-8-17-17v-4a1 1 0 0 0-2 0v4c0 10 8 18 18 19v13H21a1 1 0 0 0 0 2h22a1 1 0 0 0 0-2H33V49c10-1 18-9 18-19v-4l-1-1z" />
            </svg>
          )}
        </button>
        <button className="w-12 aspect-square rounded-full flex items-center justify-center bg-gray-200">
          <HiOutlinePhoneMissedCall className="h-3/4 w-3/4 " />
        </button>
      </div>
    </div>
  );
}
