import { Room } from "../types";

const ACTIONS = {
  SET_IS_ROOM_HOST: "SET_IS_ROOM_HOST",
  SET_ROOM_ID: "SET_ROOM_ID",
  SET_USER_ID: "SET_USER_ID",
  SET_PARTICIPANTS: "SET_PARTICIPANT",
};

export const setIsRoomHost = (isRoomHost: boolean) => {
  return {
    type: ACTIONS.SET_IS_ROOM_HOST,
    payload: { isRoomHost },
  };
};

export const setUserId = (userId: string) => {
  return {
    type: ACTIONS.SET_USER_ID,
    payload: { userId },
  };
};

export const setRoomId = (roomId: string) => {
  return {
    type: ACTIONS.SET_ROOM_ID,
    payload: { roomId },
  };
};

export const setParticipants = (participants: Room["participants"]) => {
  return {
    type: ACTIONS.SET_PARTICIPANTS,
    payload: { participants },
  };
};

export default ACTIONS;
