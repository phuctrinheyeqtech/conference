export const CLIENT = {
  GET_NEW_USER: "get-new-user",
  GET_MY_DETAILS: "get-my-details",
  GET_EXISTING_PARTICIPANTS: "get-existing-participants",
  RECEIVE_VIDEO_ANSWER: "receive-video-answer",
  RECEIVE_SERVER_CANDIDATE: "receive-server-candidate",
};

export const SERVER = {
  CREATE_ROOM: "create-room",
  JOIN_ROOM: "join-room",
  LEAVE_ROOM: "leave-room",
  RECEIVE_CLIENT_CANDIDATE: "receive-client-candidate",
  CONNECT_MEDIA: "connect-media",
};
