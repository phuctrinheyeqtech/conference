import * as React from "react";

import { Routes, Route } from "react-router-dom";
import { useSocket } from "./hooks/useSocket";
import AdminPage from "./pages/admin";
import HomePage from "./pages/home";
import RoomPage from "./pages/room";


function App() {
  useSocket();
  return (
    <Routes>
      <Route path="/">
        <Route index element={<HomePage />} />
        <Route path="admin" element={<AdminPage />} />
        <Route path="room">
          <Route path=":roomId" element={<RoomPage />} />
        </Route>
      </Route>
    </Routes>
  );
}

export default App;
